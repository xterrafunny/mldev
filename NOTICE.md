# License Notice

Copyright (C) 2020-2022 MLRep Team & Contributors.  All rights reserved.

Except where noted otherwise, this software is licensed under the Apache
License, Version 2.0 (the "License"); you may not use this work except in
compliance with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## MLRep Team
- Anton Khritankov (@se_ml)
- Valentin Polezhaev (@valen7in)
- Mugadzhir Akaev (@abyki)
- Alexey Kirilishin (@avkirilishin1)
- Anton Sotnikov (@Antosha1)

## Contributors
- Nikita Pershin (@yodusk)
- Ruslan Koshkin (@ruslanbk11) 
- Artem Uhov (@uhov.aa)
- Nikita Uhov (@uhov.na)
- Aleksandr Severinov (@aleksandrseverinov)
- Irina Kudryasheva (@kudryashovaia)
- Irina Nifantova (@NifantovaIrina)
- Andrey Belikov 
- Ivan Nasedkin
- Ananga Thapaliya (@donrast41)
- Danyil Dvorianov (@DanProgrammer1999)

## Third-party licences

This software depends on third-party libraries, 
see files src/*/requirements.txt for the full list.
